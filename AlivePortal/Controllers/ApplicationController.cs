﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlivePortal.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace AlivePortal.Controllers
{
    public class ApplicationController : Controller
    {
        private Entities db = new Entities();
        //
        // GET: /User/

        public ActionResult UploadIcon(int id)
        {
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.id = application.id;
          
            }
            return View(applicationViewModel);
        }
        public ActionResult UploadFile(int id)
        {
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.id = application.id;

            }
            return View(applicationViewModel);
        }
        public ActionResult UploadPhoneScreen(int id)
        {
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.id = application.id;

            }
            return View(applicationViewModel);
        }
        public ActionResult UploadTabletScreen(int id)
        {
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.id = application.id;

            }
            return View(applicationViewModel);
        }
          [Authorize]
        public ActionResult List()
        {
            return View();
        }
        public JsonResult GetApplications()
        {

            return Json(db.Applications.Select(c => new { Id = c.id, Name = c.title }), JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveIcon(IEnumerable<HttpPostedFileBase> icon,string id)
        {
            // The Name of the Upload component is "files"
            int app_id = int.Parse(id);
            Application application = db.Applications.SingleOrDefault(i => i.id == app_id);
             if (application != null)
             {

                 if (icon != null)
                 {
                     foreach (HttpPostedFileBase file in icon)
                     {
                            var binary = new byte[file.ContentLength];
                            file.InputStream.Read(binary, 0, file.ContentLength);
                             application.iconfile =  binary;
                             db.SaveChanges();
                     }
                 }
             }
            // Return an empty string to signify success
            return Content("");
        }
        public ActionResult SavePhoneScreen(IEnumerable<HttpPostedFileBase> images, string id)
        {
            // The Name of the Upload component is "files"
            int app_id = int.Parse(id);
            Application application = db.Applications.SingleOrDefault(i => i.id == app_id);
            if (application != null)
            {

                if (images != null)
                {
                    foreach (HttpPostedFileBase file in images)
                    {
                        int imagecount = application.PhoneScreenShots.Count;
                        if (imagecount > 4)
                        {
                            PhoneScreenShot removephoneScreenShot = db.PhoneScreenShots.Where(p => p.applicationId == app_id).OrderBy(p => p.id).SingleOrDefault();
                            if (removephoneScreenShot != null)
                                application.PhoneScreenShots.Remove(removephoneScreenShot);
                        }
                        PhoneScreenShot phoneScreenShot = new PhoneScreenShot();
                      
                        var binary = new byte[file.ContentLength];
                        file.InputStream.Read(binary, 0, file.ContentLength);
                         phoneScreenShot.imgData =  binary;
                         application.PhoneScreenShots.Add(phoneScreenShot);
                        db.SaveChanges();
                    }
                }
            }
            // Return an empty string to signify success
            return Content("");
        }
        public ActionResult SaveTabletScreen(IEnumerable<HttpPostedFileBase> images, string id)
        {
            // The Name of the Upload component is "files"
            int app_id = int.Parse(id);
            Application application = db.Applications.SingleOrDefault(i => i.id == app_id);
            if (application != null)
            {

                if (images != null)
                {
                    foreach (HttpPostedFileBase file in images)
                    {
                        int imagecount = application.PhoneScreenShots.Count;
                        if (imagecount > 4)
                        {
                            TabletScreenShot removetabletScreenShot = db.TabletScreenShots.Where(p => p.applicationId == app_id).OrderBy(p => p.id).SingleOrDefault();
                            if (removetabletScreenShot != null)
                                application.TabletScreenShots.Remove(removetabletScreenShot);
                        }
                        TabletScreenShot tabletScreenShot = new TabletScreenShot();

                        var binary = new byte[file.ContentLength];
                        file.InputStream.Read(binary, 0, file.ContentLength);
                        tabletScreenShot.imgData = binary;
                        application.TabletScreenShots.Add(tabletScreenShot);
                        db.SaveChanges();
                    }
                }
            }
            // Return an empty string to signify success
            return Content("");
        }
        public ActionResult SaveFile(IEnumerable<HttpPostedFileBase> files, string id)
        {
            // The Name of the Upload component is "files"
            int app_id = int.Parse(id);
            Application application = db.Applications.SingleOrDefault(i => i.id == app_id);
            if (application != null)
            {

                if (files != null)
                {
                    foreach (HttpPostedFileBase file in files)
                    {
                        var binary = new byte[file.ContentLength];
                        file.InputStream.Read(binary, 0, file.ContentLength);
                        application.bundle_file = binary;
                        application.bundle_file_size = file.ContentLength;
                        db.SaveChanges();
                    }
                }
            }
            // Return an empty string to signify success
            return Content("");
        }
        public ActionResult DisplayIcon(string id)
        {
            // The Name of the Upload component is "files"
            int app_id = int.Parse(id);
            Application application = db.Applications.SingleOrDefault(i => i.id == app_id);
            if (application != null)
            {

                if (application.iconfile == null)
                    return HttpNotFound();
                byte[] imageData  = application.iconfile.ToArray();
                 return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/png");
             }
            return HttpNotFound();

        }
      
       public ActionResult DisplayTabletImage(string id)
        {
            // The Name of the Upload component is "files"
            int app_id = int.Parse(id);
            TabletScreenShot tabletScreenShot = db.TabletScreenShots.SingleOrDefault(i => i.id == app_id);
            if (tabletScreenShot != null)
            {

                byte[] imageData = tabletScreenShot.imgData.ToArray();
                return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/png");
            }
            else
            {
                var dir = Server.MapPath("/Content/Images");
                var path = Path.Combine(dir, "screenshotPortrait.png");
                return base.File(path, "image/png");
            }

        }
        public ActionResult DisplayPhoneImage(string id)
        {
            // The Name of the Upload component is "files"
            int app_id = int.Parse(id);
            PhoneScreenShot phoneScreenShot = db.PhoneScreenShots.SingleOrDefault(i => i.id == app_id);
            if (phoneScreenShot != null)
            {

                byte[] imageData = phoneScreenShot.imgData.ToArray();
                return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/png");
            }
            else
            {
                var dir = Server.MapPath("/Content/Images");
                var path = Path.Combine(dir, "screenshotPortrait.png");
                return base.File(path, "image/png");
            }

        }
        public ActionResult DownloadIPA(string id)
        {
            // The Name of the Upload component is "files"
            int app_id = int.Parse(id);
            Application application = db.Applications.SingleOrDefault(i => i.id == app_id);
            if (application != null)
            {

                if (application.bundle_file == null)
                    return HttpNotFound();
                byte[] imageData = application.bundle_file.ToArray();
                return new FileStreamResult(new System.IO.MemoryStream(imageData), "application/octet-stream");
            }
            return HttpNotFound();

        }
          [Authorize]
        public ActionResult Detail(int id)
        {
            ViewBag.application_id = id;
             ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.name = application.title;
                applicationViewModel.id = application.id;
                applicationViewModel.bundle_version = application.bundle_version;
                applicationViewModel.bundle_identifier = application.bundle_identifier;
                applicationViewModel.url = string.Format("{0}application/DownloadIPA/{1}", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath, application.id);
                var phoneScreens = application.PhoneScreenShots;
                var tabletScreens = application.TabletScreenShots;
                int screenCount = 1;
                if (application.bundle_file != null)
                    applicationViewModel.appUploaded = "tick.png";
                else
                    applicationViewModel.appUploaded = "cross.png";
                foreach (PhoneScreenShot phoneScreenShot in phoneScreens)
                {
                    if (screenCount == 1)
                        applicationViewModel.phoneScreen1 = phoneScreenShot.id;
                    else if (screenCount == 2)
                        applicationViewModel.phoneScreen2 = phoneScreenShot.id;
                    else if (screenCount == 3)
                        applicationViewModel.phoneScreen3 = phoneScreenShot.id;
                    else if (screenCount == 4)
                        applicationViewModel.phoneScreen4 = phoneScreenShot.id;
                    else if (screenCount == 5)
                        applicationViewModel.phoneScreen5 = phoneScreenShot.id;
                    screenCount++;
                }
                foreach (TabletScreenShot tabletScreenShot in tabletScreens)
                {
                    if (screenCount == 1)
                        applicationViewModel.tabletScreen1 = tabletScreenShot.id;
                    else if (screenCount == 2)
                        applicationViewModel.tabletScreen2 = tabletScreenShot.id;
                    else if (screenCount == 3)
                        applicationViewModel.tabletScreen3 = tabletScreenShot.id;
                    else if (screenCount == 4)
                        applicationViewModel.tabletScreen4 = tabletScreenShot.id;
                    else if (screenCount == 5)
                        applicationViewModel.tabletScreen5 = tabletScreenShot.id;
                    screenCount++;
                }
               
            }
            return View(applicationViewModel);
        }
        public ActionResult EditDetail(int id)
        {
            ViewBag.application_id = id;
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.name = application.title;
                applicationViewModel.id = application.id;
                applicationViewModel.bundle_version = application.bundle_version;
                applicationViewModel.bundle_identifier = application.bundle_identifier;
                applicationViewModel.url = string.Format("{0}application/DownloadIPA/{1}", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath, application.id);

            }
            return PartialView(applicationViewModel);
        }
        public ActionResult ShowDetail(int id)
        {
            ViewBag.application_id = id;
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.name = application.title;
                applicationViewModel.id = application.id;
                applicationViewModel.bundle_version = application.bundle_version;
                applicationViewModel.bundle_identifier = application.bundle_identifier;
                applicationViewModel.url = string.Format("{0}application/DownloadIPA/{1}", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath, application.id);

            }
            return PartialView(applicationViewModel);
        }
        [HttpPost]
        public ActionResult SaveDetails(ApplicationViewModel model)
        {
            return View("Detail",model);    
        }
        [HttpPost]
        public ActionResult EditDetail(FormCollection collection)
        {
            int id = int.Parse(collection["id"]);
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
       
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.name = application.title;
                applicationViewModel.id = application.id;
                applicationViewModel.bundle_version = application.bundle_version;
                applicationViewModel.bundle_identifier = application.bundle_identifier;
                applicationViewModel.url = string.Format("{0}application/DownloadIPA/{1}", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath, application.id);

            }
            return PartialView(applicationViewModel);
        }
        public ActionResult Manifest(int id)
        {
            ViewBag.application_id = id;
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == id);
            if (application != null)
            {
                applicationViewModel.name = application.title;
                applicationViewModel.id = application.id;
                applicationViewModel.bundle_version = application.bundle_version;
                applicationViewModel.bundle_identifier = application.bundle_identifier;
                applicationViewModel.url = string.Format("{0}application/DownloadIPA/{1}", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath, application.id);
            }
            Response.ContentType = "text/xml";
            Response.AddHeader("content-disposition", "attachment; filename=manifest.plist");
            return PartialView(applicationViewModel);
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var applicationresult = db.Applications;

            IList<ApplicationViewModel> viewModelList = new List<ApplicationViewModel>();

            foreach (Application application in applicationresult)
            {
                ApplicationViewModel applicationmodel = new ApplicationViewModel();
                applicationmodel.name = application.title;
                applicationmodel.id = application.id;
                applicationmodel.bundle_version = application.bundle_version;
                applicationmodel.bundle_identifier = application.bundle_identifier;
          
                viewModelList.Add(applicationmodel);
            }
            return Json(viewModelList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadUserGroup([DataSourceRequest] DataSourceRequest request, int group_id)
        {
            Group group = db.Groups.SingleOrDefault(i => i.id == group_id);
            GroupViewModel groupViewModel = new GroupViewModel();
            if (group != null)
            {
                groupViewModel.name = group.name;
                foreach (UserGroup usergroup in group.UserGroups)
                {
                    UserGroupViewModel usergroupmodel = new UserGroupViewModel();
                    usergroupmodel.id = usergroup.id;
                    usergroupmodel.groupname = usergroup.Group.name;
                    usergroupmodel.groupid = (int)usergroup.groupId;
                    usergroupmodel.username = usergroup.User.name;
                    usergroupmodel.userid = usergroup.User.id;

                    groupViewModel.usergroups.Add(usergroupmodel);
                }

            }
            return Json(groupViewModel.usergroups.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateGroup([DataSourceRequest] DataSourceRequest request, ApplicationGroupViewModel model, int application_id)
        {
            ApplicationGroup applicationgroup = new ApplicationGroup();
            applicationgroup.applicationId = application_id;
            applicationgroup.groupId = model.groupObject.Id;

            db.ApplicationGroups.Add(applicationgroup);
            db.SaveChanges();
            model.id = applicationgroup.id;
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult DestroyUserGroup([DataSourceRequest] DataSourceRequest request, ApplicationGroupViewModel model)
        {

            ApplicationGroup applicationgroup = db.ApplicationGroups.SingleOrDefault(i => i.id == model.id);
            if (applicationgroup != null)
            {
                db.ApplicationGroups.Remove(applicationgroup);
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, ApplicationViewModel model, HttpPostedFileBase icon, HttpPostedFileBase ipa)
        {

            if (ModelState.IsValid)
            {
                Application application = new Application();
                application.title = model.name;
                application.description = model.description;
                application.bundle_identifier = model.bundle_identifier;
                application.bundle_version = model.bundle_version;
                db.Applications.Add(application);
                db.SaveChanges();

            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, ApplicationViewModel model, IEnumerable<HttpPostedFileBase> icon, IEnumerable<HttpPostedFileBase> ipa)
        {

            if (ModelState.IsValid)
            {
                Application application = db.Applications.SingleOrDefault(i => i.id == model.id);
                if (application != null)
                {

                    application.title = model.name;
                    application.description = model.description;
                    application.bundle_identifier = model.bundle_identifier;
                    application.bundle_version = model.bundle_version;
                    db.SaveChanges();
                }

            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, GroupViewModel model)
        {

            Group group = db.Groups.SingleOrDefault(i => i.id == model.id);
            if (group != null)
            {
                db.Groups.Remove(group);
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult ReadGroup([DataSourceRequest] DataSourceRequest request, int application_id)
        {
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            Application application = db.Applications.SingleOrDefault(i => i.id == application_id);
            if (application != null)
            {
                foreach (ApplicationGroup applicationGroup in application.ApplicationGroups)
                {
                    ApplicationGroupViewModel applicationGroupViewModel = new ApplicationGroupViewModel();
                    applicationGroupViewModel.id = applicationGroup.id;
                    applicationGroupViewModel.groupname = applicationGroup.Group.name;
                    applicationGroupViewModel.groupid = (int)applicationGroup.groupId;
                    applicationGroupViewModel.applicationName = applicationGroup.Application.title;
                    applicationGroupViewModel.applicationId = applicationGroup.Application.id;

                    applicationViewModel.applicationgroups.Add(applicationGroupViewModel);
                }

            }
            return Json(applicationViewModel.applicationgroups.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDeviceType()
        {

            return Json(db.Devices.Select(c => new { Id = c.id, Name = c.name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStoreType()
        {

            return Json(db.Stores.Select(c => new { Id = c.id, Name = c.name }), JsonRequestBehavior.AllowGet);
        }
    }
}

