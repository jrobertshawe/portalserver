﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlivePortal.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;

namespace AlivePortal.Controllers
{
    public class UserController : Controller
    {
        private Entities db = new Entities();
        //
        // GET: /User/
          [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult List()
        {
            return View();
        }
           [HttpPost]
        public ActionResult Logon(FormCollection collection)
           {
               string json = collection["json"];
            if (json == null)
                return Json("failure", JsonRequestBehavior.AllowGet);
            UserRequestModelObject requestItem = JsonConvert.DeserializeObject<UserRequestModelObject>(json);
            UserSignonResponseModel response = new UserSignonResponseModel();
            response.status.error = 1;
            response.status.errorMsg = "failed";
            User user = db.Users.SingleOrDefault(i => i.userid == requestItem.username && i.password == requestItem.password);
            if (user != null)
            {
                var applicationresult = db.vUserGroupApplications.Where(p => p.userId == user.id);

              
                foreach (vUserGroupApplication application in applicationresult)
                {
                    DateTime updated_dt = DateTime.Now;
                    if (application.updated_at != null)
                        updated_dt = (DateTime)application.updated_at;
                    ApplicationResponseModel applicationmodel = new ApplicationResponseModel();
                    applicationmodel.bundle_identifier = application.title;
                    applicationmodel.bundle_content_type = "application/octel-stream"; 
                    applicationmodel.bundle_file_name = application.title;
                    applicationmodel.bundle_file_size = (int)application.bundle_file_size;
                    applicationmodel.bundle_identifier = application.bundle_identifier;
                    applicationmodel.bundle_updated_at = updated_dt.ToString("o");
                    applicationmodel.bundle_version = application.bundle_version;
                    applicationmodel.created_at = updated_dt.ToString("o");
                    applicationmodel.icon_content_type = "image/png";
                    applicationmodel.icon_file_name = "icon_2x.png";
                    applicationmodel.icon_file_size = 0;
                    applicationmodel.icon_updated_at = updated_dt.ToString("o");
                    applicationmodel.icon_url  = string.Format("{0}application/DisplayIcon/{1}", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath, application.id);
                    applicationmodel.manifest_url =  string.Format("{0}application/Manifest/{1}", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath, application.id);
                    applicationmodel.protocol = application.title;
                    applicationmodel.updated_at = updated_dt.ToString("o");
                    response.applications.Add(applicationmodel);
                }
            }
        
            return Json(response, JsonRequestBehavior.AllowGet);
        }
          [Authorize]
        public ActionResult Detail(int id)
        {
            ViewBag.user_id = id;
             User user = db.Users.SingleOrDefault(i => i.id == id);
             UserViewModel userViewModel = new UserViewModel();
            if (user != null)
             {
                 userViewModel.name = user.name;
                 userViewModel.email = user.email;
                 userViewModel.userid = user.userid;
                 foreach (UserGroup usergroup in user.UserGroups)
                 {
                     UserGroupViewModel usergroupmodel = new UserGroupViewModel();
                     usergroupmodel.id = usergroup.id;
                     usergroupmodel.groupname = usergroup.Group.name;
                     usergroupmodel.groupid = (int)usergroup.groupId;
                     usergroupmodel.username = usergroup.User.name;
                     usergroupmodel.userid = usergroup.User.id;

                     userViewModel.usergroups.Add(usergroupmodel);
                 }

             }
             return View(userViewModel);
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var usersresult = db.Users;

            IList<UserViewModel> viewModelList = new List<UserViewModel>();

            foreach (User user in usersresult)
            {
                UserViewModel usermodel = new UserViewModel();
                usermodel.id = user.id;
                usermodel.name = user.name;
                usermodel.userid = user.userid;
                usermodel.password = user.password;
                usermodel.email = user.email;

                viewModelList.Add(usermodel);
            }
            return Json(viewModelList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadApplication([DataSourceRequest] DataSourceRequest request, int user_id)
        {
            var applicationresult = db.vUserGroupApplications.Where(p => p.userId == user_id);

            IList<ApplicationViewModel> viewModelList = new List<ApplicationViewModel>();

            foreach (vUserGroupApplication application in applicationresult)
            {
                ApplicationViewModel applicationmodel = new ApplicationViewModel();
                applicationmodel.name = application.title;
                applicationmodel.id = application.id;
                viewModelList.Add(applicationmodel);
            }
            return Json(viewModelList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGroups()
        {

            return Json(db.Groups.Select(c => new { Id = c.id, Name = c.name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUsers()
        {

            return Json(db.Users.Select(c => new { Id = c.id, Name = c.name }), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadGroup([DataSourceRequest] DataSourceRequest request, int id)
        {
            var usersresult = db.Users;

            IList<UserViewModel> viewModelList = new List<UserViewModel>();

            foreach (User user in usersresult)
            {
                UserViewModel usermodel = new UserViewModel();
                usermodel.name = user.name;
                usermodel.userid = user.userid;
                usermodel.password = user.password;
                usermodel.email = user.email;

                viewModelList.Add(usermodel);
            }
            return Json(viewModelList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadUserGroup([DataSourceRequest] DataSourceRequest request, int user_id)
        {
            User user = db.Users.SingleOrDefault(i => i.id == user_id);
            UserViewModel userViewModel = new UserViewModel();
            if (user != null)
            {
                userViewModel.name = user.name;
                userViewModel.email = user.email;
                userViewModel.userid = user.userid;
                foreach (UserGroup usergroup in user.UserGroups)
                {
                    UserGroupViewModel usergroupmodel = new UserGroupViewModel();
                    usergroupmodel.id = usergroup.id;
                    usergroupmodel.groupname = usergroup.Group.name;
                    usergroupmodel.groupid = (int)usergroup.groupId;
                    usergroupmodel.username = usergroup.User.name;
                    usergroupmodel.userid = usergroup.User.id;

                    userViewModel.usergroups.Add(usergroupmodel);
                }

            }
            return Json(userViewModel.usergroups.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateUserGroup([DataSourceRequest] DataSourceRequest request, UserGroupViewModel model, int user_id)
        {
            UserGroup usergroup = new UserGroup();
            usergroup.userId = user_id;
            usergroup.groupId = model.groupObject.Id;

            db.UserGroups.Add(usergroup);
            db.SaveChanges();
            model.id = usergroup.id;
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult DestroyUserGroup([DataSourceRequest] DataSourceRequest request, UserGroupViewModel model)
        {

            UserGroup usergroup = db.UserGroups.SingleOrDefault(i => i.id == model.id);
            if (usergroup != null)
            {
                db.UserGroups.Remove(usergroup);
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, UserViewModel model)
        {

            if (ModelState.IsValid)
            {
                User user = new User();
                user.name = model.name;
                user.userid = model.userid;

                user.password = model.password;
                user.email = model.email;
                db.Users.Add(user);
                db.SaveChanges();

            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, UserViewModel model)
        {

            if (ModelState.IsValid)
            {
                    User user = db.Users.SingleOrDefault(i => i.id == model.id);
                    if (user != null)
                    {

                        user.name = model.name;
                        user.userid = model.userid;

                        user.password = model.password;
                        user.email = model.email;
                        db.SaveChanges();
                    }

            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, UserViewModel model)
        {

            User user = db.Users.SingleOrDefault(i => i.id == model.id);
            if (user != null)
            {
                db.Users.Remove(user);
                  db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
    }
}
