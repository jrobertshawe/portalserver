﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlivePortal.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace AlivePortal.Controllers
{
    public class GroupController : Controller
    {
        private Entities db = new Entities();
        //
        // GET: /User/

        public ActionResult Index()
        {
            return View();
        }
          [Authorize]
        public ActionResult List()
        {
            return View();
        }
          [Authorize]
        public ActionResult Detail(int id)
        {
            ViewBag.group_id = id;
            Group group = db.Groups.SingleOrDefault(i => i.id == id);
            GroupViewModel groupViewModel = new GroupViewModel();
            if (group != null)
            {
                groupViewModel.name = group.name;
                foreach (UserGroup usergroup in group.UserGroups)
                {
                    UserGroupViewModel usergroupmodel = new UserGroupViewModel();
                    usergroupmodel.id = usergroup.id;
                    usergroupmodel.groupname = usergroup.Group.name;
                    usergroupmodel.groupid = (int)usergroup.groupId;
                    usergroupmodel.username = usergroup.User.name;
                    usergroupmodel.userid = usergroup.User.id;

                    groupViewModel.usergroups.Add(usergroupmodel);
                }

            }
            return View(groupViewModel);
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var groupresult = db.Groups;

            IList<GroupViewModel> viewModelList = new List<GroupViewModel>();

            foreach (Group group in groupresult)
            {
                GroupViewModel groupmodel = new GroupViewModel();
                groupmodel.name = group.name;
                groupmodel.id = group.id;
                viewModelList.Add(groupmodel);
            }
            return Json(viewModelList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadUserGroup([DataSourceRequest] DataSourceRequest request, int group_id)
        {
            Group group = db.Groups.SingleOrDefault(i => i.id == group_id);
            GroupViewModel groupViewModel = new GroupViewModel();
            if (group != null)
            {
                groupViewModel.name = group.name;
                foreach (UserGroup usergroup in group.UserGroups)
                {
                    UserGroupViewModel usergroupmodel = new UserGroupViewModel();
                    usergroupmodel.id = usergroup.id;
                    usergroupmodel.groupname = usergroup.Group.name;
                    usergroupmodel.groupid = (int)usergroup.groupId;
                    usergroupmodel.username = usergroup.User.name;
                    usergroupmodel.userid = usergroup.User.id;

                    groupViewModel.usergroups.Add(usergroupmodel);
                }

            }
            return Json(groupViewModel.usergroups.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadApplication([DataSourceRequest] DataSourceRequest request, int group_id)
        {
            var applicationresult = db.vGroupApplications.Where(p => p.groupId == group_id);

            IList<ApplicationViewModel> viewModelList = new List<ApplicationViewModel>();

            foreach (vGroupApplication application in applicationresult)
            {
                ApplicationViewModel applicationmodel = new ApplicationViewModel();
                applicationmodel.name = application.title;
                applicationmodel.id = application.id;
                applicationmodel.bundle_version = application.bundle_version;
                applicationmodel.bundle_identifier = application.bundle_identifier;
                viewModelList.Add(applicationmodel);
            }
            return Json(viewModelList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateApplication([DataSourceRequest] DataSourceRequest request, ApplicationViewModel model, int group_id)
        {
            ApplicationGroup applicationGroup = new ApplicationGroup();
            applicationGroup.groupId = group_id;
            applicationGroup.applicationId = model.applicationObject.Id;

            db.ApplicationGroups.Add(applicationGroup);
            db.SaveChanges();
            model.id = applicationGroup.id;
            Application application = db.Applications.SingleOrDefault(i => i.id == model.applicationObject.Id);
               if (application != null)
               {
                   model.name = application.title;
                   model.bundle_version = application.bundle_version;
                   model.bundle_identifier = application.bundle_identifier;
               }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateUserGroup([DataSourceRequest] DataSourceRequest request, UserGroupViewModel model, int group_id)
        {
            UserGroup usergroup = new UserGroup();
            usergroup.groupId = group_id;
            usergroup.userId = model.userObject.Id;

            db.UserGroups.Add(usergroup);
            db.SaveChanges();
            model.id = usergroup.id;
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult DestroyUserGroup([DataSourceRequest] DataSourceRequest request, UserGroupViewModel model)
        {

            UserGroup usergroup = db.UserGroups.SingleOrDefault(i => i.id == model.id);
            if (usergroup != null)
            {
                db.UserGroups.Remove(usergroup);
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, GroupViewModel model)
        {

            if (ModelState.IsValid)
            {
                Group group = new Group();
                group.name = model.name;

                db.Groups.Add(group);
                db.SaveChanges();

            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, GroupViewModel model)
        {

            if (ModelState.IsValid)
            {
                Group group = db.Groups.SingleOrDefault(i => i.id == model.id);
                if (group != null)
                {

                    group.name = model.name;

                
                    db.SaveChanges();
                }

            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, GroupViewModel model)
        {

            Group group = db.Groups.SingleOrDefault(i => i.id == model.id);
            if (group != null)
            {
                db.Groups.Remove(group);
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
    }
}
