﻿using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Web.Mvc;
using System.Web.Security;
using System;
using System.ComponentModel.DataAnnotations;
namespace AlivePortal.Models
{
    public class UserRequestModelObject
    {
        public string username { get; set; }
        public string password { get; set; }
       

    }
    public class StatusResponseModel
    {

        public string errorMsg { get; set; }
        public int error { get; set; }
    }
   
    [System.Serializable]
    public class GroupObject
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
        
    }
    [System.Serializable]
    public class ApplicationObject
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

    }
    [System.Serializable]
    public class UserObject
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

    }
     [System.Serializable]
     public class UserViewModel
    {
        public int id { get; set; }
        public string userid { get; set; }
        public string name  { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public IList<UserGroupViewModel> usergroups { get; set; }
        public IList<ApplicationViewModel> applications { get; set; }
        public UserViewModel()
        {
            usergroups = new List<UserGroupViewModel>();
            applications = new List<ApplicationViewModel>();
        }

    }
     [System.Serializable]
     public class GroupViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public IList<UserGroupViewModel> usergroups { get; set; }
        public IList<ApplicationViewModel> applications { get; set; }
        public GroupViewModel()
        {
            usergroups = new List<UserGroupViewModel>();
            applications = new List<ApplicationViewModel>();
    
        }
      
    }
     [System.Serializable]
     public class ApplicationViewModel
     {
         public int id { get; set; }
            [DisplayName("Title")]
         public string name { get; set; }
            [DisplayName("Description")]
            [DataType(DataType.MultilineText)]
         public string description { get; set; }
            [DisplayName("Identify")]
         public string bundle_identifier { get; set; }
          [DisplayName("Version")]
         public string bundle_version { get; set; }
          public string updateDateString { get; set; }
            [DisplayName("Developer")]
          public string developer { get; set; }
           [DisplayName("Device")]
            public string device { get; set; }
           [DisplayName("Store")]
           public string store { get; set; }
           public int phoneScreen1 { get; set; }
           public int phoneScreen2 { get; set; }
           public int phoneScreen3 { get; set; }
           public int phoneScreen4 { get; set; }
           public int phoneScreen5 { get; set; }
           public int tabletScreen1 { get; set; }
           public int tabletScreen2 { get; set; }
           public int tabletScreen3 { get; set; }
           public int tabletScreen4 { get; set; }
           public int tabletScreen5 { get; set; }
          public string url { get; set; }
          public string appUploaded { get; set; }
          public GroupObject groupObject { get; set; }
          public ApplicationObject applicationObject { get; set; }
    
         public IList<ApplicationGroupViewModel> applicationgroups { get; set; }
         public ApplicationViewModel()
         {
             applicationgroups = new List<ApplicationGroupViewModel>();
         }

     }
     [System.Serializable]
     public class ApplicationResponseModel
     {
         public string bundle_content_type { get; set; }
         public string bundle_file_name { get; set; }
             public int bundle_file_size { get; set; }
             public string bundle_identifier { get; set; }
             public string bundle_updated_at { get; set; }
             public string bundle_version { get; set; }
             public string created_at { get; set; }
             public string group_id { get; set; }
             public string icon_content_type { get; set; }
             public string icon_file_name { get; set; }
             public int icon_file_size { get; set; }
             public string icon_updated_at { get; set; }
             public string icon_url { get; set; }
              public string manifest_url { get; set; }
             public string protocol { get; set; }
             public string updated_at { get; set; }
     }
     [System.Serializable]
     public class UserGroupViewModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public int userid { get; set; }
        public string groupname { get; set; }
        public int groupid { get; set; }
        public GroupObject groupObject { get; set; }
        public UserObject userObject { get; set; }
        public ApplicationObject applicationObject { get; set; }
    }
         [System.Serializable]
     public class ApplicationGroupViewModel
    {
        public int id { get; set; }
        public string applicationName { get; set; }
        public int applicationId { get; set; }
        public string groupname { get; set; }
        public int groupid { get; set; }
        public GroupObject groupObject { get; set; }
        public UserObject userObject { get; set; }
    }
        [System.Serializable]
         public class UserSignonResponseModel
         {
             public StatusResponseModel status { get; set; }
             public IList<ApplicationResponseModel> applications { get; set; }

             public UserSignonResponseModel()
             {

                 //    response = new responseModel();
                 status = new StatusResponseModel();
                 applications = new List<ApplicationResponseModel>();

             }
         }
}