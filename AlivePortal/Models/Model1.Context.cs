﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AlivePortal.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Entities : DbContext
    {
        public Entities()
            : base("name=Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<ApplicationGroup> ApplicationGroups { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<vUserGroupApplication> vUserGroupApplications { get; set; }
        public DbSet<vGroupApplication> vGroupApplications { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<PhoneScreenShot> PhoneScreenShots { get; set; }
        public DbSet<TabletScreenShot> TabletScreenShots { get; set; }
    }
}
