﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Html;
using System.Web.Mvc;
using System.Web.Routing;
using System.Linq.Expressions;

namespace AlivePortal.helpers
{
    public static class HtmlExtensions
    {
        public static HtmlString Label(this HtmlHelper helper, string target = "", string text = "", string id = "")
        {
            return new HtmlString(string.Format("<label id='{0}' for='{1}'>{2}</label>", id, target, text));
        }

        public static MvcHtmlString MenuItem(this HtmlHelper helper,
                    string linkText, string actionName, string controllerName)
        {

            string currentControllerName = (string)helper.ViewContext.RouteData.Values["controller"];
            string currentActionName = (string)helper.ViewContext.RouteData.Values["action"];
            string activeClass = "";
            var builder = new TagBuilder("li");
            if (controllerName.Equals(currentControllerName, StringComparison.CurrentCultureIgnoreCase))
                activeClass = "active";
            builder.InnerHtml = helper.ActionLink(linkText, actionName, controllerName).ToHtmlString();
            string temp = string.Format("<a   href=\"/{0}/{1}\"><span class=\"{3}\">{2}</span></a>", linkText, actionName, controllerName, activeClass);
            builder.InnerHtml = temp;
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
        }
        public static MvcHtmlString ActionLinkButton(this HtmlHelper htmlHelper, string buttonText, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            string href = UrlHelper.GenerateUrl("default", actionName, controllerName, routeValues, RouteTable.Routes, htmlHelper.ViewContext.RequestContext, false);
            string buttonHtml = string.Format("<a class=\"floatl small button toggletip tipped\" href=\"{1}\" title=\"\"><span class=\"green\">{0}</span></a>", buttonText, href);

            return new MvcHtmlString(buttonHtml);

        }
        public static MvcHtmlString ActionRedButton(this HtmlHelper htmlHelper, string buttonText, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            string href = UrlHelper.GenerateUrl("default", actionName, controllerName, routeValues, RouteTable.Routes, htmlHelper.ViewContext.RequestContext, false);
            string buttonHtml = string.Format("<a class=\"floatl small button toggletip tipped\" href=\"{1}\" title=\"\"><span class=\"red\">{0}</span></a>", buttonText, href);

            return new MvcHtmlString(buttonHtml);

        }
        public static MvcHtmlString ActionGrayButton(this HtmlHelper htmlHelper, string buttonText, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            string href = UrlHelper.GenerateUrl("default", actionName, controllerName, routeValues, RouteTable.Routes, htmlHelper.ViewContext.RequestContext, false);
            string buttonHtml = string.Format("<a class=\"floatl small button toggletip tipped\" href=\"{1}\" title=\"\"><span class=\"gray\">{0}</span></a>", buttonText, href);

            return new MvcHtmlString(buttonHtml);

        }
        public static MvcHtmlString JSGrayButton(this HtmlHelper htmlHelper, string buttonText, string Command)
        {
            string buttonHtml = string.Format("<a class=\"floatl small button toggletip tipped\" href=\"#\" title=\"\" onclick='{1}'; return false;><span class=\"gray\">{0}</span></a>", buttonText, Command);

            return new MvcHtmlString(buttonHtml);

        }
        public static MvcHtmlString ActionLinkText(this HtmlHelper htmlHelper, string buttonText, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            string href = UrlHelper.GenerateUrl("default", actionName, controllerName, routeValues, RouteTable.Routes, htmlHelper.ViewContext.RequestContext, false);
            string buttonHtml = string.Format("<a href='{1}'>{0}</a>", buttonText, href);
            return new MvcHtmlString(buttonHtml);
        }
        public static MvcHtmlString ActionEditButton(this HtmlHelper htmlHelper, string buttonText, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            string href = UrlHelper.GenerateUrl("default", actionName, controllerName, routeValues, RouteTable.Routes, htmlHelper.ViewContext.RequestContext, false);
            string buttonHtml = string.Format("<input type=\"button\"  value=\"{0}\" onclick=\"location.href='{1}'\" class=\"editButton\" />", buttonText, href);
            return new MvcHtmlString(buttonHtml);
        }
        public static MvcHtmlString ActionDeleteButton(this HtmlHelper htmlHelper, string buttonText, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            string href = UrlHelper.GenerateUrl("default", actionName, controllerName, routeValues, RouteTable.Routes, htmlHelper.ViewContext.RequestContext, false);
            string buttonHtml = string.Format("<input type=\"button\"  value=\"{0}\" onclick=\"location.href='{1}'\" class=\"deleteButton\" />", buttonText, href);
            return new MvcHtmlString(buttonHtml);
        }
        public static MvcHtmlString GetDisplayName<TModel, TProperty>(
        this HtmlHelper<TModel> htmlHelper,
        Expression<Func<TModel, TProperty>> expression
    )
        {
            var metaData = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string value = metaData.DisplayName ?? (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));
            string buttonHtml = string.Format("<span class=\"label\">{0}</span>", value);


            return MvcHtmlString.Create(buttonHtml);
        }
    }
    
}
